import { useEffect, useState } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import iconClose from '../../assets/fi_x.png';
import axios from 'axios';

export const SignInComp = ({ closeLogin, setSignupAccess, handleLoginPage }) => {
	const [validated, setValidated] = useState(false);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [submittedForm, setSubmittedForm] = useState({});
	const [hasNotRegistered, setHasNotRegistered] = useState(false);
	const [wrongPass, setWrongPass] = useState(false);

	async function signIn() {
		try {
			const payload = { email, password };
			const response = await axios.post(
				'https://bootcamp-rent-cars.herokuapp.com/customer/auth/login',
				payload
			);
			const userLog = response.data;
			if (response.status === 201) {
				localStorage.setItem('email', userLog.email);
				localStorage.setItem('cliAcCsTKn', userLog.access_token);
				handleLoginPage();
			}
		} catch (err) {
			console.error(err);
			const errorStatus = err.response.status;
			if (errorStatus === 404) setHasNotRegistered(true);
			if (errorStatus === 400) setWrongPass(true);
		}
	}

	const submitSignIn = (event) => {
		event.preventDefault();
		const form = event.currentTarget;
		!!form.checkValidity() && signIn();

		setSubmittedForm({ email, password });
		setValidated(true);
	};

	if (!!hasNotRegistered && email !== submittedForm.email) setHasNotRegistered(false);
	if (!!wrongPass && password !== submittedForm.password) setWrongPass(false);

	return (
		<div className="w-100 position-relative" style={{ maxWidth: '370px', background: 'white' }}>
			<img
				src={iconClose}
				alt="close"
				className="position-absolute d-md-none"
				style={{ top: '1.75rem', right: 0, cursor: 'pointer' }}
				onClick={closeLogin}
			/>
			<div className="binar-logo" />
			<h1
				style={{
					fontFamily: 'Arial',
					fontWeight: 700,
					fontSize: '1.5rem',
					lineHeight: 1.5,
					marginBottom: '2rem',
				}}
			>
				Welcome Back!
			</h1>
			<Form noValidate validated={validated} onSubmit={submitSignIn}>
				<Form.Group controlId="email">
					<Form.Label className="login__font">Email</Form.Label>
					<Form.Control
						type="email"
						placeholder="Contoh:johndee@gmail.com"
						required
						className={`login__placeholder ${hasNotRegistered && 'tab--invalid'}`}
						onChange={(e) => setEmail(e.target.value)}
					></Form.Control>
					{!!hasNotRegistered && (
						<p className="mt-1 mb-0" style={{ fontSize: '0.875rem', color: '#dc3545' }}>
							User was not registered
						</p>
					)}
					<Form.Control.Feedback type="invalid">
						Please provide a valid email.
					</Form.Control.Feedback>
				</Form.Group>
				<Form.Group controlId="password" className="mt-3">
					<Form.Label className="login__font">Password</Form.Label>
					<Form.Control
						type="password"
						minLength="6"
						placeholder="6+ karakter"
						required
						className={`login__placeholder ${
							(hasNotRegistered || wrongPass) && 'tab--invalid'
						}`}
						onChange={(e) => setPassword(e.target.value)}
					></Form.Control>
					{!!wrongPass && (
						<p className="mt-1 mb-0" style={{ fontSize: '0.875rem', color: '#dc3545' }}>
							Wrong password!
						</p>
					)}
					<Form.Control.Feedback type="invalid">
						Please provide 6 characters password at minimum.
					</Form.Control.Feedback>
				</Form.Group>
				<Button
					type="submit"
					className="w-100"
					style={{ borderRadius: '2px', background: '#0D28A6', margin: '2rem 0' }}
				>
					Sign in
				</Button>
			</Form>
			<p className="login__font text-center">
				Don't have an account?
				<u
					style={{ fontWeight: '700', color: '#0D28A6', cursor: 'pointer' }}
					onClick={() => setSignupAccess(true)}
				>
					Sign Up for free
				</u>
			</p>
		</div>
	);
};

export const SignUpComp = ({ closeLogin, setSignupAccess }) => {
	const [validated, setValidated] = useState(false);
	const [username, setUsername] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isRegistered, setIsRegistered] = useState(false);
	const [submittedForm, setSubmittedForm] = useState({});
	const [emailExisted, setEmailExisted] = useState(false);
	const [number, setNumber] = useState(7);

	async function signUp() {
		try {
			const payload = { email, password, role: 'Customer' };
			const response = await axios.post(
				'https://bootcamp-rent-cars.herokuapp.com/customer/auth/register',
				payload
			);
			if (response.status === 201) setIsRegistered(true);
		} catch (err) {
			console.error(err);
			if (err.response.status === 400) setEmailExisted(true);
		}
	}

	const submitSignUp = (event) => {
		event.preventDefault();
		const form = event.currentTarget;
		!!form.checkValidity() && signUp();

		setSubmittedForm({ username, email, password });
		setValidated(true);
	};

	if (!!emailExisted && email !== submittedForm.email) setEmailExisted(false);

	useEffect(() => {
		if (!!isRegistered) {
			const countdown = setInterval(() => {
				setNumber((prev) => prev - 1);
			}, 1000);

			setTimeout(() => {
				clearInterval(countdown);
				setNumber(7);
				setIsRegistered(false);
				setSignupAccess(false);
			}, 7000);

			return () => {
				clearInterval(countdown);
				setNumber(7);
			};
		}
	}, [isRegistered, setSignupAccess]);

	return (
		<div className="w-100 position-relative" style={{ maxWidth: '370px', background: 'white' }}>
			<img
				src={iconClose}
				alt="close"
				className="position-absolute d-md-none"
				style={{ top: '1.75rem', right: 0 }}
				onClick={closeLogin}
			/>
			<div className="binar-logo"></div>
			<h1
				style={{
					fontFamily: 'Arial',
					fontWeight: 700,
					fontSize: '1.5rem',
					lineHeight: 1.5,
					marginBottom: '2rem',
				}}
			>
				Sign Up
			</h1>
			{!!isRegistered && (
				<p className="login_font text-success" style={{ marginTop: '-1rem' }}>
					Your account has been successfully created. will&nbsp;redirect to sign-in form
					in <b>{number}</b>
				</p>
			)}
			<Form noValidate validated={validated} onSubmit={submitSignUp}>
				<fieldset disabled={!!isRegistered}>
					<Form.Group controlId="username">
						<Form.Label className="login__font">Name*</Form.Label>
						<Form.Control
							type="text"
							placeholder="Full Name"
							className={`login__placeholder ${emailExisted && 'tab--invalid'}`}
							required
							onChange={(e) => setUsername(e.target.value)}
						></Form.Control>
						<Form.Control.Feedback type="invalid">
							Please provide your name.
						</Form.Control.Feedback>
					</Form.Group>
					<Form.Group controlId="email" className="mt-3">
						<Form.Label className="login__font">Email*</Form.Label>
						<Form.Control
							type="email"
							placeholder="Contoh:johndee@gmail.com"
							className={`login__placeholder ${emailExisted && 'tab--invalid'}`}
							required
							onChange={(e) => setEmail(e.target.value)}
						></Form.Control>
						{!!emailExisted && (
							<p
								className="mt-1 mb-0"
								style={{ fontSize: '0.875rem', color: '#dc3545' }}
							>
								This email address is already registered
							</p>
						)}
						<Form.Control.Feedback type="invalid">
							Please provide a valid email.
						</Form.Control.Feedback>
					</Form.Group>
					<Form.Group controlId="password" className="mt-3">
						<Form.Label className="login__font">Create Password</Form.Label>
						<Form.Control
							type="password"
							minLength="6"
							placeholder="6+ karakter"
							className={`login__placeholder ${emailExisted && 'tab--invalid'}`}
							required
							onChange={(e) => setPassword(e.target.value)}
						></Form.Control>
						<Form.Control.Feedback type="invalid">
							Please provide 6 characters password at minimum.
						</Form.Control.Feedback>
					</Form.Group>
					<Button
						type="submit"
						className="w-100"
						style={{ borderRadius: '2px', background: '#0D28A6', margin: '2rem 0' }}
					>
						SignUp
					</Button>
				</fieldset>
			</Form>
			<p className="login__font text-center">
				Already have an account?
				<u
					style={{ fontWeight: 700, color: '#0D28A6', cursor: 'pointer' }}
					onClick={() => setSignupAccess(false)}
				>
					Sign In here
				</u>
			</p>
		</div>
	);
};
