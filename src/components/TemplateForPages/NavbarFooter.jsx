import React, { useState } from "react";
import { useNavigate } from 'react-router-dom';
import { Navbar, Container, Row, Col, Offcanvas, Nav } from 'react-bootstrap';
import facebook from '../../assets/icon_facebook.png';
import instagram from '../../assets/icon_instagram.png';
import twitter from '../../assets/icon_twitter.png';
import email from '../../assets/icon_mail.png';
import twitch from '../../assets/icon_twitch.png';
import './NavbarFooter.css';


export const Navigation = ({ handleLoginPage }) => {
	const [showSidebar, setShowSidebar] = useState(false);
	const clientToken = localStorage.getItem('cliAcCsTKn');
	const navigate = useNavigate();

	const logOut = () => {
		localStorage.removeItem('cliAcCsTKn');
		navigate('/');
	};

  return (
		<>
			<Navbar expand="md" className="navbar mb-0 justify-content-center w-100">
				<Container>
					<Navbar.Brand className="brand"></Navbar.Brand>
					<Navbar.Toggle
						aria-controls={`offcanvasNavbar-expand`}
						onClick={() => setShowSidebar(true)}
					/>
					<Navbar.Offcanvas
						show={showSidebar}
						onHide={() => setShowSidebar(false)}
						id={`offcanvasNavbar-expand`}
						tabIndex="-1"
						aria-labelledby={`offcanvasNavbarLabel-expand`}
						placement="end"
					>
						<Offcanvas.Header closeButton>
							<Offcanvas.Title id={`offcanvasNavbarLabel-expand`}>
								BCR
							</Offcanvas.Title>
						</Offcanvas.Header>

						<Offcanvas.Body>
							<Nav className="justify-content-end flex-grow-1 pe-3">
								<Nav.Link href="/#service">Our Services</Nav.Link>
								<Nav.Link href="/#why">Why Us</Nav.Link>
								<Nav.Link href="/#testimoni">Testimonial</Nav.Link>
								<Nav.Link href="/#question">FAQ</Nav.Link>
							</Nav>
							{clientToken === null ? (
								<button
									className="register btn-success"
									onClick={() => {
										setShowSidebar(false);
										handleLoginPage();
									}}
								>
									Register
								</button>
							) : (
								<button
									className="btn btn-secondary d-flex align-items-center"
									style={{ height: '36px', borderRadius: '2px' }}
									onClick={logOut}
								>
									Log out
								</button>
							)}
						</Offcanvas.Body>
					</Navbar.Offcanvas>
				</Container>
			</Navbar>
		</>
  );
}

export const Footer = () => {
	return (
		<Container className="footer">
			<Row>
				<Col className="address col-lg-3 d-flex flex-column">
					<div className="mb-3">
						Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000
					</div>
					<div className="mb-3">binarcarrental@gmail.com</div>
					<div className="mb-3">081-233-334-808</div>
				</Col>
				<Col className="col-lg-2 d-flex flex-column">
					<a href="#ourservices" className="mb-3 link">
						Our services
					</a>
					<a href="#whyus" className="mb-3 link">
						Why Us
					</a>
					<a href="#testimonial" className="mb-3 link">
						Testimonial
					</a>
					<a href="#faq" className="mb-3 link">
						FAQ
					</a>
				</Col>
				<Col className="col-lg-3 d-flex flex-column">
					<div className="mb-3 connect">Connect with us</div>
					<div className="d-flex flex-row mb-3">
						<img className="icon" src={facebook} alt="" />
						<img className="icon" src={instagram} alt="" />
						<img className="icon" src={twitter} alt="" />
						<img className="icon" src={email} alt="" />
						<img className="icon" src={twitch} alt="" />
					</div>
				</Col>
				<Col className="col-lg-3 d-flex flex-column">
					<div className="mb-3 copyright">Copyright Binar 2022</div>
					<div className="mb-3 logo_binar"></div>
				</Col>
			</Row>
		</Container>
	);
};

