import Button from 'react-bootstrap/Button';
import { Col, Row } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import HeroImg from '../assets/img_car.png';


const Hero = ({ addButton }) => {
    const navigate = useNavigate()
	return (
		<div
			id="hero"
			className="hero position-relative d-flex"
			style={{ height: 'auto', minHeight: '440px', background: '#f1f3ff' }}
		>
			<div className="container d-flex align-items-center">
				<Row>
					<Col md={6}>
						<h1 className="fw-bold mt-4">
							Sewa & Rental Mobil Terbaik di kawasan (Lokasimu)
						</h1>
						<p className="my-4" style={{ width: '80%' }}>
							Selamat datang di Binar Car Rental. Kami menyediakan mobil kualitas
							terbaik dengan harga terjangkau. Selalu siap melayani kebutuhanmu untuk
							sewa mobil selama 24 jam.
						</p>
						{addButton && (
							<Button
								className="px-4 py-2 main-button"
								variant="primary"
								style={{ background: '#5cb85f', border: 'none', color: 'white' }}
								onClick={() => navigate('/cars')}
							>
								Mulai Sewa Mobil
							</Button>
						)}
					</Col>
					<Col md={6} className="position-xs-relative position-md-absolute bottom-0 end-0">
                        <div className='w-100 d-block d-md-none' style={{height:'52vw'}}/>
						<img src={HeroImg} alt="" className="position-absolute bottom-0 end-0" />
					</Col>
				</Row>
			</div>
		</div>
	);
};

export default Hero
