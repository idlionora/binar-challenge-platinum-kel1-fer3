import { useState } from 'react';
import { Outlet } from 'react-router-dom';
import { SignInComp, SignUpComp } from './TemplateForPages/LogIn';
import './TemplateForPages/LogIn.css';
import {Footer, Navigation} from './TemplateForPages/NavbarFooter';

const TemplateForPages = () => {
	const [loginAccess, setLoginAccess] = useState(false);
	const [mainPageClass, setMainPageClass] = useState('');
	const [signupAccess, setSignupAccess] = useState(true);

	const handleLoginPage = () => {
		if (!loginAccess) {
			setMainPageClass('main-page--absolute');
			setTimeout(() => {
				setMainPageClass('main-page--absolute main-page--transform');
			}, 10);
			setLoginAccess(true);
		} else {
			setMainPageClass('main-page--absolute');
			setTimeout(() => {
				setMainPageClass('');
				setLoginAccess(false);
			}, 500);
		}
	};

	const closeLogin = () => {
		setMainPageClass('');
		setLoginAccess(false);
	};

	return (
		<div
			className={`container-fluid p-0 position-relative ${
				!!loginAccess && 'overflow-hidden'
			}`}
			style={{ height: '100vh' }}
		>
			<div
				className="col-md-6 position-absolute h-100"
				style={{ background: '#0D28A6', right: 0 }}
			>
				<p
					className="position-absolute text-white text-nowrap"
					style={{
						fontWeight: 500,
						fontSize: '48px',
						lineHeight: 1.5,
						opacity: 0.7,
						top: '12.5%',
						left: '16%',
					}}
				>
					Binar Car Rental
				</p>
			</div>
			<div className="col-12 col-md-6 d-flex justify-content-center align-items-sm-center h-100 px-3 position-absolute">
				{!!loginAccess && !signupAccess && (
					<SignInComp
						closeLogin={closeLogin}
						setSignupAccess={setSignupAccess}
						handleLoginPage={handleLoginPage}
					/>
				)}
				{!!loginAccess && !!signupAccess && (
					<SignUpComp closeLogin={closeLogin} setSignupAccess={setSignupAccess} />
				)}
			</div>
			<div
				className={`main-page w-100 bg-white position-relative ${mainPageClass}`}
				style={{ minHeight: '100vh' }}
				onClick={!!loginAccess ? handleLoginPage : null}
			>
				<Navigation handleLoginPage={handleLoginPage}/>
				<div aria-hidden={!!loginAccess ? 'true' : 'false'}>
					<Outlet context={handleLoginPage} />
				</div>
				<Footer/>
			</div>
		</div>
	);
};

export default TemplateForPages;
