const mockResponse = {
	id: 1,
	total_price: 500000,
	start_rent_at: '2022-12-08',
	finish_rent_at: '2022-12-12',
	User: {
		id: 1,
		email: 'fain@bcr.io',
		role: 'Admin',
	},
	Car: {
		id: 1,
		name: 'Toyota Avanza',
		category: 'medium',
		price: 100000,
	},
};

export default {
	get: jest.fn().mockResolvedValue(mockResponse)
};
