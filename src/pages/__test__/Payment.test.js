import { render, screen } from "@testing-library/react"
import { Provider } from "react-redux"
import { BrowserRouter } from "react-router-dom";
import store from '../../redux/store';
import Payment from "../Payment"

const MockPayment = () => {
    return (
		<BrowserRouter>
			<Provider store={store}>
				<Payment />
			</Provider>
		</BrowserRouter>
	);
}

describe("Payment Page", () => {
    describe("car detail info", () => {
        it('should render label of car name', async () => {
			render(<MockPayment />);
			const carNameText = await screen.findByText(/nama\/tipe mobil/i);
            expect(carNameText).toBeInTheDocument();
		});

        it('should render label of category', async () => {
            render(<MockPayment />);
			const categoryText = await screen.findByText(/kategori/i);
			expect(categoryText).toBeInTheDocument();
        })

        it('should render label of start_rent_at', async () => {
			render(<MockPayment />);
			const startDateText = await screen.findByText(/tanggal mulai sewa/i);
			expect(startDateText).toBeInTheDocument();
		});

        it('should render label of finish_rent_at', async () => {
			render(<MockPayment />);
			const endDateText = await screen.findByText(/tanggal akhir sewa/i);
			expect(endDateText).toBeInTheDocument();
		});
    })  

    describe("bank transfer buttons", () => {
        it('should have three different options', async () => {
            render(<MockPayment />);
            const bankButtons = await screen.findAllByRole("radio")
            expect(bankButtons.length).toBe(3)
        })

        it('should render label for BCA', async () => {
            render(<MockPayment />);
            const bcaLabel = await screen.findByText(/bca transfer/i);
            expect(bcaLabel).toBeInTheDocument();
        })
        
        it('should render label for BNI', async () => {
			render(<MockPayment />);
			const bniLabel = await screen.findByText(/bni transfer/i);
			expect(bniLabel).toBeInTheDocument();
		});

        it('should render label for Mandiri', async () => {
			render(<MockPayment />);
			const mandiriLabel = await screen.findByText(/mandiri transfer/i);
			expect(mandiriLabel).toBeInTheDocument();
		});
    })

    describe('payment info and button', () => {
		it('should display total_price', async () => {
			render(<MockPayment />);
			const priceText = await screen.findByTestId("totalPrice");
			expect(priceText).toBeInTheDocument();
		});

		it('should provide payment button', async () => {
			render(<MockPayment />);
			const paymentButton = await screen.findByTestId('paymentButton');
			expect(paymentButton).toBeInTheDocument();
		});
	});
})
