import axios from 'axios';
import { useEffect, useRef, useState } from 'react';
import { useNavigate, useParams, useOutletContext } from 'react-router-dom';
import BlankFormCar from '../components/FormCar/BlankFormCar';
import { Card, Container, Row, Col, Accordion, Form, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser } from '@fortawesome/free-regular-svg-icons';
import 'react-date-range/dist/styles.css'; // main style file
import 'react-date-range/dist/theme/default.css'; // theme css file
import { DateRange } from 'react-date-range';
import { addDays, format } from 'date-fns';
import { useDispatch, useSelector } from 'react-redux';
import './CarDetail.css';

const CarDetail = () => {
	const navigate = useNavigate();
	const [cars, setCars] = useState([]);
	const { id } = useParams();
	const [range, setRange] = useState([
		{
			startDate: new Date(),
			endDate: addDays(new Date(), 7),
			key: 'selection',
		},
	]);
	let rentDateDifference = range[0].endDate.getTime() - range[0].startDate.getTime();
	let rentDays = Math.ceil(rentDateDifference / (1000 * 3600 * 24)) + 1;
	const handleLoginPage = useOutletContext();
	const refOne = useRef(null);
	const [open, setOpen] = useState(false);
	const dispatch = useDispatch();
	const stateGlobal = useSelector(state => state);

	// eslint-disable-next-line react-hooks/exhaustive-deps
	const getCars = async () => {
		try {
			const response = await axios.get(
				`https://bootcamp-rent-cars.herokuapp.com/customer/car/${id}`
			);
			const data = response.data;
			setCars(data);
			dispatch({type: 'UPDATE_GETS_CAR_DATA', payload: data})
		} catch (err) {
			console.log(err);
		}
	};

	const hideOnClickOutside = (e) => {
		if (refOne.current && !refOne.current.contains(e.target)) {
			setOpen(false);
		}
	};

	useEffect(() => {
		getCars();
		document.addEventListener('click', hideOnClickOutside, true);
	}, []);

	const createOrder = async () => {
		try {
			const payload = {
				start_rent_at: format(range[0].startDate, 'yyy-MM-dd'),
				finish_rent_at: format(range[0].endDate, 'yyy-MM-dd'),
				car_id: id,
			};
			const response = await axios.post(
				'https://bootcamp-rent-cars.herokuapp.com/customer/order',
				payload,
				{
					headers: {
						access_token: localStorage.getItem('cliAcCsTKn'),
					},
				}
			);
			if (response.status === 201) {navigate('/cars/payment/');
			dispatch({type: 'UPDATE_CAR_DATE', payload: response.data})};
		} catch (err) {
			console.error(err);
		}
	};

	const checkToken = () => {
		if (localStorage.getItem('cliAcCsTKn') !== null) {
			createOrder();
		} else {
			handleLoginPage();
		}
	};

	return (
		<>
			<div className="w-100" style={{ background: '#F1F3FF', height: '210px' }} />
			<Container>
				<BlankFormCar />
				<Row className="pt-4 ps-3">
					<Col className="mb-4">
						<Card>
							<Card.Body>
								<h5>
									<strong>Tentang Paket</strong>
								</h5>
								<h5 className="mt-3">
									<strong>Includes</strong>
								</h5>
								<ul>
									<li>
										Apa saja yang termasuk dalam paket misal durasi max 12 jam
									</li>
									<li>Sudah termasuk bensin selama 12 jam</li>
									<li>Sudah termasuk Tiket Wisata</li>
									<li>Sudah termasuk pajak</li>
								</ul>
								<h5 className="mt-3">
									<strong>Excludes</strong>
								</h5>
								<ul>
									<li>Tidak termasuk biaya makan sopir Rp 75000/hari</li>
									<li>
										Jika overtime lebih dari 12 jam akan ada tambahan biaya Rp
										20.000/jam
									</li>
									<li>Tidak termasuk okomodasi penginapan</li>
								</ul>
								<Accordion>
									<Accordion.Header>
										<h5 className="mt-3">
											<strong>Refund, Reschedule, Overtime</strong>
										</h5>
									</Accordion.Header>
									<Accordion.Body>
										<ul>
											<li> Tidak termasuk biaya makan sopir Rp75.000/hari</li>
											<li>
												{' '}
												Jika overtime lebih dari 12 jam akan ada tambahan
												biaya Rp20.000/jam
											</li>
											<li> Tidak termasuk akomodasi penginapan</li>
											<li> Tidak termasuk biaya makan sopir Rp75.000/hari</li>
											<li>
												{' '}
												Jika overtime lebih dari 12 jam akan ada tambahan
												biaya Rp20.000/jam
											</li>
											<li> Tidak termasuk akomodasi penginapan</li>
											<li> Tidak termasuk biaya makan sopir Rp75.000/hari</li>
											<li>
												{' '}
												Jika overtime lebih dari 12 jam akan ada tambahan
												biaya Rp20.000/jam
											</li>
											<li> Tidak termasuk akomodasi penginapan</li>
										</ul>
									</Accordion.Body>
								</Accordion>
							</Card.Body>
						</Card>
					</Col>

					<Col>
						<Card>
							<Card.Img variant="top" src={cars.image} />
							<Card.Body>
								<Card.Title>
									<h5 className="mt-3">
										<strong>{cars.name}</strong>
									</h5>
									{cars.category === 'small' ? (
										<h5>
											<small>
												<FontAwesomeIcon icon={faUser} /> 4-6 orang
											</small>
										</h5>
									) : cars.category === 'medium' ? (
										<h5>
											<small>
												<FontAwesomeIcon icon={faUser} /> 6-8 orang
											</small>
										</h5>
									) : (
										<h5>
											<small>
												<FontAwesomeIcon icon={faUser} /> 8-10 orang
											</small>
										</h5>
									)}
								</Card.Title>
								<Row className="mt-5">
									<Col>
										<h5 className="mt-3">
											Tentukan Lama Sewa Mobil (max. 7 hari)
										</h5>
									</Col>
								</Row>
								<Row className="mt-1">
									<Col md={12}>
										<Form.Control
											value={`${format(
												range[0].startDate,
												'MM/dd/yyy'
											)} to ${format(range[0].endDate, 'MM/dd/yyy')}`}
											readOnly
											className="inputBox"
											onClick={() => setOpen((open) => !open)}
										/>
										<div ref={refOne}>
											{open && (
												<DateRange
													editableDateInputs={true}
													onChange={(date) => setRange([date.selection])}
													moveRangeOnFirstSelection={false}
													ranges={range}
													months={1}
													direction="horizontal"
												/>
											)}
										</div>
									</Col>
								</Row>
								<Row className="mt-5 d-flex justify-content-between">
									<h5 className="mt-3" style={{ width: 'fit-content' }}>
										<strong>Total</strong>
									</h5>
									<h5 className="mt-3" style={{ width: 'fit-content' }}>
										<strong>
											Rp 
											{(cars.price * rentDays).toLocaleString('id-ID')}
										</strong>
									</h5>
								</Row>
								<Row className="mt-3" style={{ padding: '0 0.75rem' }}>
									<Button variant="success" size="lg" onClick={checkToken} disabled={rentDays > 7 ? true : false}>
										Lanjutkan Pembayaran
									</Button>
								</Row>
							</Card.Body>
						</Card>
					</Col>
				</Row>
			</Container>
		</>
	);
};

export default CarDetail;
