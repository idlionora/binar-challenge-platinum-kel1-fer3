import React from 'react';
import Button from 'react-bootstrap/Button';
import ImgService from '../assets/img_service.png';
import Check from '../assets/check.png';
import Jempol from '../assets/jempol.png';
import PriceIco from '../assets/icon_price.png';
import Ico24Hrs from '../assets/icon_24hrs.png';
import IcoProfesional from '../assets/icon_professional.png';
import Card from 'react-bootstrap/Card';
import ImgProfile from '../assets/img_photo.png';
import Rate from '../assets/Rate.png';
import Carousel from 'react-bootstrap/Carousel';
import Accordion from 'react-bootstrap/Accordion';
import { Col, Row } from 'react-bootstrap';
import Hero from '../components/Hero';
import './Home.css';

const Home = () => {
	return (
		<div id="homePage">
			<Hero addButton/>
			<div id="service" className="service my-5">
				<div className="container">
					<Row className="d-flex align-items-center justify-content-center">
						<Col md={5}>
							<img src={ImgService} alt="" className="img-service" />
						</Col>
						<Col md={5}>
							<h3 className="fw-bold">
								Best Car Rental for any kind of trip in (Lokasimu)!
							</h3>
							<p className="my-4">
								Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga
								lebih murah dibandingkan yang lain, kondisi mobil baru, serta
								kualitas pelayanan terbaik untuk perjalanan wisata, bisnis, wedding,
								meeting, dll.
							</p>
							<ul className="list-service list-unstyled">
								<li className="d-flex gap-2">
									<img src={Check} alt="" />
									<p>Sewa Mobil Dengan Supir di Bali 12 Jam</p>
								</li>
								<li className="d-flex gap-2">
									<img src={Check} alt="" />
									<p>Sewa Mobil Lepas Kunci di Bali 24 Jam</p>
								</li>
								<li className="d-flex gap-2">
									<img src={Check} alt="" />
									<p>Sewa Mobil Jangka Panjang Bulanan</p>
								</li>
								<li className="d-flex gap-2">
									<img src={Check} alt="" />
									<p>Gratis Antar - Jemput Mobil di Bandara</p>
								</li>
								<li className="d-flex gap-2">
									<img src={Check} alt="" />
									<p>Layanan Airport Transfer / Drop In Out</p>
								</li>
							</ul>
						</Col>
					</Row>
				</div>
			</div>

			<div id="why" className="why my-5">
				<div className="container">
					<div className="title">
						<h3 className="fw-bold">Why Us?</h3>
						<p>Mengapa harus pilih Binar Car Rental?</p>
					</div>
					<div className="card-why">
						<Row>
							<Col md={3} className="mb-4">
								<Card>
									<Card.Body>
										<img className="img" src={Jempol} alt="" />
										<Card.Title className="my-2 fw-bold">
											Mobil Lengkap
										</Card.Title>
										<Card.Text>
											Tersedia banyak pilihan mobil, kondisi masih baru,
											bersih dan terawat dengan kondisi mesin prima.
										</Card.Text>
									</Card.Body>
								</Card>
							</Col>
							<Col md={3} className=" mb-4">
								<Card>
									<Card.Body>
										<img className="img" src={PriceIco} alt="" />
										<Card.Title className="my-2 fw-bold">
											Harga Murah
										</Card.Title>
										<Card.Text>
											Harga murah dan bersaing, bisa bandingkan harga kami
											dengan rental mobil lain.
										</Card.Text>
									</Card.Body>
								</Card>
							</Col>
							<Col md={3} className=" mb-4">
								<Card>
									<Card.Body>
										<img className="img" src={Ico24Hrs} alt="" />
										<Card.Title className="my-2 fw-bold">
											Layanan 24 Jam
										</Card.Title>
										<Card.Text>
											Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami
											juga tersedia di akhir minggu.
										</Card.Text>
									</Card.Body>
								</Card>
							</Col>
							<Col md={3} className=" mb-4">
								<Card>
									<Card.Body>
										<img className="img" src={IcoProfesional} alt="" />
										<Card.Title className="my-2 fw-bold">
											Sopir Profesional
										</Card.Title>
										<Card.Text>
											Sopir yang profesional, berpengalaman, jujur, ramah dan
											selalu tepat waktu.
										</Card.Text>
									</Card.Body>
								</Card>
							</Col>
						</Row>
					</div>
				</div>
			</div>

			<div id="testimoni" className="testimoni my-5">
				<div className="container">
					<div className="title text-center">
						<h3 className="fw-bold">Testimonial</h3>
						<p>Berbagai review positif dari para pelanggan kami</p>
					</div>
					<Carousel>
						<Carousel.Item>
							<Row className="d-flex align-items-center p-5">
								<Col md={1}>
									<img src={ImgProfile} alt="" />
								</Col>
								<Col md={11}>
									<img src={Rate} alt="" />
									<p>
										“Lorem ipsum dolor sit amet, consectetur adipiscing elit,
										sed do eiusmod lorem ipsum dolor sit amet, consectetur
										adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
										consectetur adipiscing elit, sed do eiusmod”
									</p>
									<p className="fw-light">John Dee 32, Bromo</p>
								</Col>
							</Row>
						</Carousel.Item>
						<Carousel.Item>
							<Row className="d-flex align-items-center p-5">
								<Col md={1}>
									<img src={ImgProfile} alt="" />
								</Col>
								<Col md={11}>
									<img src={Rate} alt="" />
									<p>
										“Lorem ipsum dolor sit amet, consectetur adipiscing elit,
										sed do eiusmod lorem ipsum dolor sit amet, consectetur
										adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
										consectetur adipiscing elit, sed do eiusmod”
									</p>
									<p className="fw-light">John Dee 32, Bromo</p>
								</Col>
							</Row>
						</Carousel.Item>
						<Carousel.Item>
							<Row className="d-flex align-items-center p-5">
								<Col md={1}>
									<img src={ImgProfile} alt="" />
								</Col>
								<Col md={11}>
									<img src={Rate} alt="" />
									<p>
										“Lorem ipsum dolor sit amet, consectetur adipiscing elit,
										sed do eiusmod lorem ipsum dolor sit amet, consectetur
										adipiscing elit, sed do eiusmod lorem ipsum dolor sit amet,
										consectetur adipiscing elit, sed do eiusmod”
									</p>
									<p className="fw-light">John Dee 32, Bromo</p>
								</Col>
							</Row>
						</Carousel.Item>
					</Carousel>
				</div>
			</div>

			<div id="banner" className="banner">
				<div className="container">
					<div className="item text-center rounded">
						<h1>Sewa Mobil di (Lokasimu) Sekarang</h1>
						<p className="mt-3 mb-4">
							Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua.
						</p>
						<Button className="px-4 py-2 main-button" variant="primary">
							Mulai Sewa Mobil
						</Button>
					</div>
				</div>
			</div>

			<div id="question" className="question my-5">
				<div className="container">
					<Row className="d-flex justify-content-between">
						<Col md={3}>
							<h4 className="wf-bold">Frequently Asked Question</h4>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit</p>
						</Col>
						<Col md={7}>
							<Accordion>
								<Accordion.Item eventKey="0">
									<Accordion.Header>
										Apa saja syarat yang dibutuhkan?
									</Accordion.Header>
									<Accordion.Body>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
										do eiusmod tempor incididunt ut labore et dolore magna
										aliqua. Ut enim ad minim veniam, quis nostrud exercitation
										ullamco laboris nisi ut aliquip ex ea commodo consequat.
										Duis aute irure dolor in reprehenderit in voluptate velit
										esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
										occaecat cupidatat non proident, sunt in culpa qui officia
										deserunt mollit anim id est laborum.
									</Accordion.Body>
								</Accordion.Item>
								<Accordion.Item eventKey="1">
									<Accordion.Header>
										Berapa hari minimal sewa mobil lepas kunci?
									</Accordion.Header>
									<Accordion.Body>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
										do eiusmod tempor incididunt ut labore et dolore magna
										aliqua. Ut enim ad minim veniam, quis nostrud exercitation
										ullamco laboris nisi ut aliquip ex ea commodo consequat.
										Duis aute irure dolor in reprehenderit in voluptate velit
										esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
										occaecat cupidatat non proident, sunt in culpa qui officia
										deserunt mollit anim id est laborum.
									</Accordion.Body>
								</Accordion.Item>
								<Accordion.Item eventKey="2">
									<Accordion.Header>
										Berapa hari sebelumnya sabaiknya booking sewa mobil?
									</Accordion.Header>
									<Accordion.Body>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
										do eiusmod tempor incididunt ut labore et dolore magna
										aliqua. Ut enim ad minim veniam, quis nostrud exercitation
										ullamco laboris nisi ut aliquip ex ea commodo consequat.
										Duis aute irure dolor in reprehenderit in voluptate velit
										esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
										occaecat cupidatat non proident, sunt in culpa qui officia
										deserunt mollit anim id est laborum.
									</Accordion.Body>
								</Accordion.Item>
								<Accordion.Item eventKey="3">
									<Accordion.Header>
										Apakah Ada biaya antar-jemput?
									</Accordion.Header>
									<Accordion.Body>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
										do eiusmod tempor incididunt ut labore et dolore magna
										aliqua. Ut enim ad minim veniam, quis nostrud exercitation
										ullamco laboris nisi ut aliquip ex ea commodo consequat.
										Duis aute irure dolor in reprehenderit in voluptate velit
										esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
										occaecat cupidatat non proident, sunt in culpa qui officia
										deserunt mollit anim id est laborum.
									</Accordion.Body>
								</Accordion.Item>
								<Accordion.Item eventKey="4">
									<Accordion.Header>
										Bagaimana jika terjadi kecelakaan
									</Accordion.Header>
									<Accordion.Body>
										Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
										do eiusmod tempor incididunt ut labore et dolore magna
										aliqua. Ut enim ad minim veniam, quis nostrud exercitation
										ullamco laboris nisi ut aliquip ex ea commodo consequat.
										Duis aute irure dolor in reprehenderit in voluptate velit
										esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
										occaecat cupidatat non proident, sunt in culpa qui officia
										deserunt mollit anim id est laborum.
									</Accordion.Body>
								</Accordion.Item>
							</Accordion>
						</Col>
					</Row>
				</div>
			</div>
		</div>
	);
};

export default Home;
