import LeftNarrow from '../assets/fi_arrow-left.png';
import PaymentStage from '../assets/paymentstage.png'
import './ProofPayment.css'
import bca_image from '../assets/bca.png'
import bni_image from '../assets/bni.png'
import mandiri_image from '../assets/mandiri.png'
import fi_copy from '../assets/fi_copy.png'
import React, {useState, useEffect} from "react";
import { Link, useNavigate} from "react-router-dom";
import axios from "axios";
import { useSelector } from "react-redux";

const ProofPayment = () => {
    const navigate = useNavigate();

    const stateGlobal = useSelector(state => state);

    var getTimePayment = localStorage.getItem("StartPayment", "");
    var startPayment = new Date(getTimePayment);
    var endPayment = new Date(startPayment);
    endPayment.setDate(startPayment.getDate()+1);
    const endPaymentDate = (endPayment) => {
        const date = new Date(endPayment);
        return date.toLocaleString('id', {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
          });
        }
    var endPaymentHour = endPayment.getHours();
    var endPaymentMinute = endPayment.getMinutes();

    var timePaymentNow = new Date();
    var diff = timePaymentNow.getTime()-endPayment.getTime();
    var countdownTimer = Math.abs(diff)

    const [time, setTime] = useState(countdownTimer);
    const [timeProof, setTimeProof] = useState(240000)

    useEffect(() => {
        const mytimeout = setTimeout(() => {
            setTime(time - 1000);
        }, 1000);
        if(time < 1){
            clearTimeout(mytimeout)
            alert("Waktu Pembayaran Telah Habis");
        }
    }, [time]);

    useEffect(() => {
        const mytimeout = setTimeout(() => {
            setTimeProof(timeProof - 1000);
        }, 1000);
        if(timeProof < 1){
            clearTimeout(mytimeout)
            alert("Waktu Upload Bukti Pembayaran Telah Habis");
        }
    }, [timeProof]);



    const getFormattedTime = (milliseconds) => {
        let total_seconds = parseInt(Math.floor(milliseconds / 1000));
        let total_minutes = parseInt(Math.floor(total_seconds / 60));
        let total_hours = parseInt(Math.floor(total_minutes / 60));

        let seconds = parseInt(total_seconds % 60);
        let minutes = parseInt(total_minutes % 60);
        let hours = parseInt(total_hours % 24);
        
        return `${hours} : ${minutes} : ${seconds}`;
       
    }

    const getTimeProof = (milliseconds) => {
        let total_seconds = parseInt(Math.floor(milliseconds / 1000));
        let total_minutes = parseInt(Math.floor(total_seconds / 60));

        let seconds = parseInt(total_seconds % 60);
        let minutes = parseInt(total_minutes % 60);

        return `${minutes} : ${seconds}`;
    }

    const [selectImage, setImage]= useState('')

    function handleImage(e){
        setImage(e.target.files[0])
    }

    function handleURLImageAPI(){
        let formData = new FormData();
        formData.append("slip", selectImage);

        axios.put(`https://bootcamp-rent-cars.herokuapp.com/customer/order/${stateGlobal.updateCarDate.id}/slip`, formData, {
            headers: {
                "Content-Type": "multipart/form-data",
                "access_token": localStorage.getItem('cliAcCsTKn')
            }
        }).then((response) => {
            navigate(`/ticket/${stateGlobal.updateCarDate.id}`);
            alert("Pembayaran Berhasil");
          }).catch((error) => {
            console.log(error)
          })
         
    }

    return (
        <>
        <div className="container-paymentconfirm d-flex flex-column">
            <div className="d-flex justify-content-center" style={{height: "110px", backgroundColor: "#F1F3FF"}}>
                <div className="d-flex flex-row h-100 justify-content-between align-items-end" style={{width: '75%'}}>
                    <div className="d-flex flex-column align-items-start justify-content-end" >
                        <div className="d-flex flex-row align-items-center" style={{height: "3rem"}}>
                            <img src={LeftNarrow} className="btn btn-link img-leftnarrow p-0" alt="leftnarrow"/>
                            <p className="fw-bold mt-3 ms-3">Pembayaran</p>   
                        </div>
                        <p className="" style={{fontSize: '12px', marginLeft: '41px'}}>Order ID: {stateGlobal.updateGetsCarData.id}</p>         
                    </div>
                    <div className="d-flex align-items-center h-100">
                        <img src={PaymentStage} alt="frameprogress"/>
                    </div>
                </div>              
            </div>
            <div className="d-flex justify-content-center mt-5">
                <div className="d-flex flex-row"  style={{width: '75%'}}>
                    <div className="d-flex flex-column col-7 me-4">
                        <div className="countdown-container d-flex flex-row p-3 justify-content-between align-items-center">
                            <div className="d-flex flex-column p-0">
                                <p className="fw-bold" style={{fontSize: '14px'}}>Selesaikan Pembayaran Sebelum</p>
                                <p className="mb-0" style={{fontSize: '12px'}}>{endPaymentDate} jam {endPaymentHour} : {endPaymentMinute} WIB</p>
                            </div>
                            <div className="fw-bold mt-2">{getFormattedTime(time)}</div> 
                        </div>
                        <div className="transferpayment-container d-flex flex-column mt-4 p-3">
                            <div className="d-flex flex-column p-0">
                                <p className="fw-bold" style={{fontSize: '14px'}}>Lakukan transfer ke</p>
                                <div className="d-flex flex-row mb-3">
                                    {stateGlobal.BankTransfer === 'BCA' ? (<img className="me-3" src={bca_image} alt="bca" style={{width: '61px', height: '30px'}}/>) : 
                                            stateGlobal.BankTransfer === 'BNI' ? (<img className="me-3" src={bni_image} alt="bni" style={{width: '61px', height: '30px'}}/>) : 
                                            (<img className="me-3" src={mandiri_image} alt="mandiri" style={{width: '61px', height: '30px'}}/>)}
                                    <div className="d-flex flex-column">
                                        <label>
                                            {stateGlobal.BankTransfer === 'BCA' ? (<p>BCA Transfer</p>) : 
                                            stateGlobal.BankTransfer === 'BNI' ? (<p className="mb-0">BNI Transfer</p>) : 
                                            (<p className="mb-0">Mandiri Transfer</p>)}</label>
                                        <label style={{fontSize: '12px'}}>a.n Binar Car Rental</label>
                                    </div>
                                </div>
                                <div className="d-flex flex-column mb-3">
                                    <p className="mb-1" style={{fontSize: '12px'}}>Nomor Rekening</p>
                                    <div className="rekeningField d-flex flex-row justify-content-between p-2">
                                        <p className="mb-0" style={{fontSize: '12px'}}>
                                            {stateGlobal.BankTransfer === 'BCA' ? (<p>5104257877</p>) : 
                                            stateGlobal.BankTransfer === 'BNI' ? (<p className="mb-0">2350987959</p>) : 
                                            (<p className="mb-0">88992222</p>)}</p>
                                        <img src={fi_copy} alt="fi_copy" style={{width: '18px', height: '18px'}}/>
                                    </div>
                                </div>
                                <div className="d-flex flex-column mb-2">
                                    <p className="mb-1" style={{fontSize: '12px'}}>Total Bayar</p>
                                    <div className="rekeningField d-flex flex-row justify-content-between p-2">
                                        <p className="mb-0" style={{fontSize: '12px'}}>Rp. {stateGlobal.updateCarDate.total_price}</p>
                                        <img src={fi_copy} alt="fi_copy" style={{width: '18px', height: '18px'}}/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="instruction-container d-flex flex-column mt-4 p-3">
                            <div className="d-flex flex-column p-0">
                                <p className="fw-bold" style={{fontSize: '14px'}}>Instruksi Pembayaran</p>
                            </div>
                            <div class="tabs w-100">
                                <input type="radio" class="tabs__radio" name="tabs-example" id="tab1"/>
                                <label for="tab1" class="tabs__label">ATM {stateGlobal.BankTransfer}</label>
                                <div class="tabs__content m-3 mb-0">
                                    <li>Masukkan kartu ATM, lalu PIN</li>
                                    <li>Pilih menu “Transaksi Lainnya” – ‘Transfer” – “Ke Rek {stateGlobal.BankTransfer} Virtual Account”</li>
                                    <li>Masukkan nomor {stateGlobal.BankTransfer} Virtual Account: 70020+Order ID</li>
                                    <p className="mb-0" style={{marginLeft: '19px', color: '#8A8A8A'}}>Contoh</p>
                                    <p className="mb-0" style={{marginLeft: '19px', color: '#8A8A8A'}}>No. Peserta: 12345678, maka ditulis 7002012345678</p>
                                    <li>Layar ATM akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi</li>
                                    <li>Ambil dan simpanlah bukti transaksi tersebut</li>
                                </div>
                                <input type="radio" class="tabs__radio" name="tabs-example" id="tab2"/>
                                <label for="tab2" class="tabs__label">{stateGlobal.BankTransfer === 'BCA' ? (<p>M-BCA</p>) : 
                                            stateGlobal.BankTransfer === 'BNI' ? (<p className="mb-0">BNI M-Banking</p>) : 
                                            (<p className="mb-0">Livin By Mandiri</p>)}</label>
                                <div class="tabs__content m-3 mb-0">
                                    <li>Buka Aplikasi, lalu PIN</li>
                                    <li>Pilih menu “Transaksi Lainnya” – ‘Transfer” – “Ke Rek {stateGlobal.BankTransfer} Virtual Account”</li>
                                    <li>Masukkan nomor {stateGlobal.BankTransfer} Virtual Account: 70020+Order ID</li>
                                    <p className="mb-0" style={{marginLeft: '19px', color: '#8A8A8A'}}>Contoh</p>
                                    <p className="mb-0" style={{marginLeft: '19px', color: '#8A8A8A'}}>No. Peserta: 12345678, maka ditulis 7002012345678</p>
                                    <li>Layar ATM akan menampilkan konfirmasi, ikuti instruksi untuk menyelesaikan transaksi</li>
                                    <li>Ambil dan simpanlah bukti transaksi tersebut</li></div>
                                <input type="radio" class="tabs__radio" name="tabs-example" id="tab3"/>
                                <label for="tab3" class="tabs__label">{stateGlobal.BankTransfer} Klik</label>
                                <div class="tabs__content m3- mb-0">{stateGlobal.BankTransfer} Klik</div>
                                <input type="radio" class="tabs__radio" name="tabs-example" id="tab4"/>
                                <label for="tab4" class="tabs__label">Internet Banking</label>
                                <div class="tabs__content">Internet Banking</div>
                            </div>
                        </div>
                    </div>
                    <div className="confirmPayment-container d-flex flex-column col-5 p-3">
                        <div className="d-flex flex-row justify-content-between">
                            <p className="fw-bold mb-4" style={{fontSize: '14px'}}>Konfirmasi Pembayaran</p>
                            <div className="fw-bold">{getTimeProof(timeProof)}</div>
                        </div>
                        <p style={{fontSize: '14px'}}>Terima kasih telah melakukan konfirmasi pembayaran. Pembayaranmu akan segera kami cek tunggu kurang lebih 10 menit untuk mendapatkan konfirmasi.</p>
                        <p className="mb-2" style={{fontSize: '14px'}}>Upload Bukti Pembayaran</p>
                        <p style={{fontSize: '14px'}}>Untuk membantu kami lebih cepat melakukan pengecekan. Kamu bisa upload bukti bayarmu</p>
                        <input type="file" name="file" onChange={handleImage}/>
                        <Link className="confirm-button btn btn-success fw-bold d-flex justify-content-center mt-2" onClick={handleURLImageAPI}>Upload</Link>
                    </div>
                </div>
            </div>
        </div>
        </>
    )
}

export default ProofPayment
