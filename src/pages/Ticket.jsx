import { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Container, Row } from 'react-bootstrap';
import leftArrow from '../assets/fi_arrow-left.png';
import smallCheck from '../assets/fi_check-sm.png';
import largeCheck from '../assets/fi_check-lg.png';
import iconDownload from '../assets/fi_download.png';
import iconImage from '../assets/fi_image.png';
import invoicePdf from '../assets/contoh-nota-kosong-A5.pdf';
import axios from 'axios';

const Ticket = () => {
	const [invoice, setInvoice] = useState(null);
	const [carData, setCarData] = useState({})
	const { id } = useParams();
	const blueCircle = {
		height: '14px',
		width: '14px',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
		background: '#0D28A6',
		borderRadius: '50%',
	};
	const blueSpan = {
		margin: '0 12px',
		height: '1px',
		width: '28px',
		background: '#0D28A6',
	};

	const getPaymentSlip = async () => {
		try {
			const response = await axios.get(
				`https://bootcamp-rent-cars.herokuapp.com/customer/order/${id}`,
				{
					headers: {
						access_token: localStorage.getItem('cliAcCsTKn'),
					},
				}
			);
			setCarData(response.data)
			setInvoice(invoicePdf); //if database generate invoice, insert slip here
		} catch (err) {
			console.log(err);
		}
	};

	useEffect(() => {
		getPaymentSlip();
	}, []);

	return (
		<>
			<div
				className="position-absolute w-100"
				style={{ background: '#F1F3FF', height: '100px', zIndex: '-1' }}
			/>
			<Container>
				<Row
					className="d-flex flex-column justify-content-end align-items-center"
					style={{ height: '100px' }}
				>
					<div
						className="px-3 w-100 d-flex justify-content-between align-items-start"
						style={{
							fontFamily: 'Arial',
							fontWeight: 400,
							fontSize: '0.75rem',
							lineHeight: 1.5,
						}}
					>
						<div className="d-flex">
							<div className="d-none d-xl-block" style={{ width: '4rem' }} />
							<img src={leftArrow} alt="" style={{ height: '24px' }} />
							<div className="ms-3">
								<p
									style={{
										fontWeight: 700,
										fontSize: '0.875rem',
										lineHeight: 1.7143,
										marginBottom: '0.375rem',
									}}
								>
									Tiket
								</p>
								<p>Order ID: {carData.CarId}</p>
							</div>
						</div>
						<div className="d-flex align-items-center">
							<div className="d-none d-sm-flex align-items-center">
								<div style={blueCircle}>
									<img src={smallCheck} alt="check" />
								</div>
								<p className="ms-2 mb-0">Pilih Metode</p>
								<span style={blueSpan} />
								<div style={blueCircle}>
									<img src={smallCheck} alt="check" />
								</div>
								<p className="ms-2 mb-0">Bayar</p>
							</div>
							<span style={blueSpan} />
							<div style={blueCircle}>
								<p
									className="text-white mb-0"
									style={{ fontSize: '10px', lineHeight: '14px' }}
								>
									3
								</p>
							</div>
							<p className="ms-2 mb-0">Tiket</p>
							<div className="d-none d-xl-block" style={{ width: '4rem' }} />
						</div>
					</div>
				</Row>
				<Row
					className="d-flex flex-column align-items-center"
					style={{
						fontFamily: 'Arial',
						fontWeight: 700,
						fontSize: '0.875rem',
						lineHeight: 1.42857,
						paddingRight: '12px',
						paddingLeft: '12px',
					}}
				>
					<div
						className="mb-3 rounded-circle d-flex justify-content-center align-items-center"
						style={{
							marginTop: '2.5rem',
							width: '48px',
							height: '48px',
							background: '#5CB85F',
						}}
					>
						<img src={largeCheck} alt="berhasil" />
					</div>
					<h2
						className="text-center mb-3"
						style={{
							fontWeight: 700,
							fontSize: '0.875rem',
							lineHeight: 1.42857,
						}}
					>
						Pembayaran Berhasil!
					</h2>
					<p className="text-center" style={{ fontWeight: 400, color: '#8A8A8A' }}>
						Tunjukkan invoice ini ke petugas BCR di titik temu.
					</p>
					<div
						className="mt-4 p-4 w-100 position-relative"
						style={{
							maxWidth: '605px',
							borderRadius: '8px',
							boxShadow: '0px 0px 4px rgba(0, 0, 0, 0.15)',
							marginBottom: '80px',
						}}
					>
						<a href={invoicePdf} target="_blank" rel="noreferrer" download>
							<button
								className="bg-white position-absolute"
								style={{
									right: '24px',
									top: '24px',
									fontWeight: '700',
									padding: '8px 12px',
									border: '1px solid #0D28A6',
									color: '#0D28A6',
									borderRadius: '2px',
								}}
							>
								<img src={iconDownload} alt="" style={{ marginRight: '10px' }} />
								Unduh
							</button>
						</a>
						<p>Invoice</p>
						<p className="mb-4" style={{ fontWeight: 400 }}>
							*no invoice
						</p>
						<div
							className="w-100 d-flex justify-content-center align-items-center"
							style={{
								border: '1px dashed #D0D0D0',
								borderRadius: '4px',
								background: '#EEEEEE',
								minHeight: '162px',
							}}
						>
							{invoice === null ? (
								<>
									<img className="inline-block me-3" src={iconImage} alt="" />
									<p className="mb-0" style={{ fontWeight: 400 }}>
										Mohon lakukan pembayaran terlebih dahulu
									</p>
								</>
							) : (
								<iframe
									src={`${invoicePdf}#view=fitH`}
									title="contoh invoice"
									width="100%"
									height="100%"
								/>
							)}
						</div>
					</div>
				</Row>
			</Container>
		</>
	);
};

export default Ticket;
