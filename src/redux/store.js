import { createStore } from "redux";

const initialState = {
    updateCarDate: [],
    updateGetsCarData: [],
    BankTransfer:''
}

const reducer = (state = initialState, action) => {
    if(action.type ==="BCA"){
        return{
            ...state,
            BankTransfer:'BCA'
        }
    }
    else if(action.type ==="BNI"){
        return{
            ...state,
            BankTransfer:'BNI'
        }
    }
    else if(action.type ==="MANDIRI"){
        return{
            ...state,
            BankTransfer:'MANDIRI'
        }
    }
    if(action.type ==='UPDATE_CAR_DATE'){
        return{
            ...state,
            updateCarDate: action.payload
        }
    }
    if(action.type ==='UPDATE_GETS_CAR_DATA'){
        return{
            ...state,
            updateGetsCarData: action.payload
        }
    }
    
    return state;
}

const store = createStore(reducer);

export default store;