import React from 'react';
import { Route, Routes } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import TemplateForPages from './components/TemplateForPages';
import Home from './pages/Home';
import CarList from './pages/CarList';
import CarDetail from './pages/CarDetail';
import DummyPagePayment from './pages/DummyPagePayment';
import Ticket from './pages/Ticket';
import { Provider } from 'react-redux';
import store from './redux/store';
import './App.css';
import Payment from './pages/Payment';
import PaymentConfirm from './pages/PaymentConfirm';
import ProofPayment from './pages/ProofPayment';

const App = () => {
	return (
		<Provider store={store}>
			<Routes>
				<Route path="/" element={<TemplateForPages />}>
					<Route index element={<Home />} />
					<Route path="cars" element={<CarList />} />
					<Route path="cars/:id" element={<CarDetail />} />
					<Route path="cars/payment" element={<Payment />} />
					<Route path="cars/paymentconfirm" element={<PaymentConfirm />} />
					<Route path="cars/proofpayment" element={<ProofPayment />} />
					<Route path="ticket/:id" element={<Ticket />} />
				</Route>
			</Routes>
		</Provider>
	);
};

export default App;
